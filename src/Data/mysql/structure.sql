/**
 * Database Schema Structure required by Yii2Module\Yii2Merge\MergeModule.
 * 
 * @author Anastaszor
 * @license MIT
 */
SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `merge_trustworthiness`;

SET foreign_key_checks = 1;

CREATE TABLE `merge_trustworthiness`
(
	`merge_trustworthiness_id` INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'The unique id of the record',
	`meta_created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Metadata for the creation of this record',
	`meta_updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Metadata - the last time this object was updated',
	`module_id` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The id of the related module',
	`classname` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The name of the class that is used',
	`fieldname` VARCHAR(255) NOT NULL COLLATE ascii_bin COMMENT 'The name of the field that is used',
	`trust_value` SMALLINT(3) NOT NULL DEFAULT 0 COMMENT 'The value of the trustworthiness for the considered fieldname in given class in given module',
	UNIQUE KEY `merge_trustworthiness_modclsfld` (`module_id`, `classname`, `fieldname`)
)
ENGINE=InnoDB 
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci 
COMMENT='Table to store all the merge trustworthiness values';
