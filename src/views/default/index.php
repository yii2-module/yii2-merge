<?php declare(strict_types=1);

/** @var yii\web\View $this */
/** @var string $title */

use yii\BaseYii;
use yii\helpers\Html;

$this->title = BaseYii::t('MergeModule.Controllers', 'Information Dashboard');
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?php echo $this->title; ?></h1>

<div class="row">
<div class="col-md-3">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h5>Trustworthinesses</h5>
		</div>
		<div class="panel-body">
			<?php echo Html::a(Html::encode('Trustworthinesses'), ['merge-trustworthiness-crud/index'], ['class' => 'btn btn-primary']); ?>
		</div>
	</div>
</div>

</div>
