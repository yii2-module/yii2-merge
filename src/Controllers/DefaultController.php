<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-merge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Merge\Controllers;

use yii\base\InvalidArgumentException;

/**
 * DefaultController class file.
 * 
 * This controller is the default index for the module.
 * 
 * @author Anastaszor
 */
class DefaultController extends \yii\web\Controller
{
	
	/**
	 * Default action. Displays the available crud actions.
	 * 
	 * @return string
	 * @throws InvalidArgumentException if the view file or the layout file does not exist
	 */
	public function actionIndex()
	{
		return $this->render('index');
	}
	
}
