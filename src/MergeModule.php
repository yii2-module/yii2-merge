<?php declare(strict_types=1);

/*
 * This file is part of the yii2-module/yii2-merge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Module\Yii2Merge;

use PhpExtended\Score\IntegerScore;
use PhpExtended\Score\ScoreInterface;
use yii\base\Module;
use yii\BaseYii;
use Yii2Extended\Metadata\Bundle;
use Yii2Extended\Metadata\Record;
use Yii2Module\Helper\BootstrappedModule;
use Yii2Module\Yii2Merge\Models\MergeTrustworthiness;

/**
 * MergeModule class file.
 *
 * This module is to store trustiness about various data sources when merging
 * data is needed.
 *
 * @author Anastaszor
 */
class MergeModule extends BootstrappedModule
{
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getBootstrapIconName()
	 */
	public function getBootstrapIconName() : string
	{
		return 'arrows-angle-contract';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getLabel()
	 */
	public function getLabel() : string
	{
		return BaseYii::t('MergeModule.Module', 'Merge');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Yii2Extended\Metadata\ModuleInterface::getEnabledBundles()
	 */
	public function getEnabledBundles() : array
	{
		return [
			'merge' => new Bundle(BaseYii::t('MergeModule.Module', 'Merge'), [
				'trust' => (new Record(MergeTrustworthiness::class, 'trust', BaseYii::t('MergeModule.Module', 'Trustworthiness')))->enableFullAccess(),
			]),
		];
	}
	
	// ---- ---- ---- ---- MergeModule API ---- ---- ---- ---- \\
	
	/**
	 * Gets whether the score for the module, class and field is registered.
	 * 
	 * @param Module $module
	 * @param string $className
	 * @param string $fieldname
	 * @return bool
	 */
	public function hasScore(Module $module, string $className, string $fieldname) : bool
	{
		return MergeTrustworthiness::find()->andWhere([
			'module_id' => $module->getUniqueId(),
			'classname' => $className,
			'fieldname' => $fieldname,
		])->exists();
	}
	
	/**
	 * Gets the score corresponding to the wanted module, class and field.
	 * 
	 * @param Module $module
	 * @param string $className
	 * @param string $fieldname
	 * @return ScoreInterface
	 */
	public function getScore(Module $module, string $className, string $fieldname) : ScoreInterface
	{
		$mtw = MergeTrustworthiness::findOne([
			'module_id' => $module->getUniqueId(),
			'classname' => $className,
			'fieldname' => $fieldname,
		]);
		
		return new IntegerScore(0, 255, (null === $mtw ? 0 : (int) $mtw->trust_value));
	}
	
	/**
	 * Sets the given score for a specific module, class and field.
	 * 
	 * @param Module $module
	 * @param string $className
	 * @param string $fieldname
	 * @param ScoreInterface $score
	 * @return boolean
	 * @throws \yii\db\Exception
	 */
	public function setScore(Module $module, string $className, string $fieldname, ScoreInterface $score) : bool
	{
		$mtw = MergeTrustworthiness::findOne([
			'module_id' => $module->getUniqueId(),
			'classname' => $className,
			'fieldname' => $fieldname,
		]);
		
		if(null === $mtw)
		{
			$mtw = new MergeTrustworthiness();
			$mtw->module_id = $module->getUniqueId();
			$mtw->classname = $className;
			$mtw->fieldname = $fieldname;
		}
		$mtw->trust_value = (int) (255.0 * $score->getNormalizedValue());
		
		return $mtw->save();
	}
	
}
